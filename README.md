<h1 id="schedule">Schedule</h1>
<table>
   <thead>
      <tr>
         <th style="width:10%">Proposed Date</th>
         <th style="width:10%">Week</th>
         <th style="width:10%">Day</th>
         <th style="width:10%">Module</th>
         <th style="width:10%">Topic</th>
         <th style="width:10%">Primary/Secondary Instructor</th>
         <th style="width:10%">Actual Date Taught</th>
         <th style="width:10%">MDBooks Completed</th>
         <th style="width:10%">Slides Completed</th>
         <th style="width:10%">Labs Completed</th>
      </tr>
   </thead>
   <tbody>
      <tr>
         <td>10-Aug</td>
         <td>1</td>
         <td>1</td>
         <td>Course Indoc/Equipment and Tools</td>
         <td>Course Setup</td>
         <td>All</td>
         <td>10-Aug</td>
         <td>x</td>
         <td>x</td>
         <td>x</td>
      </tr>
      <tr>
         <td>11-Aug</td>
         <td>1</td>
         <td>2</td>
         <td>Intro to Git</td>
         <td>Key Git Concepts, Creating Repositories, Branching, Version Control, Resources</td>
         <td>Curran(Lead)/Cammarata</td>
         <td>11-Aug</td>
         <td>x</td>
         <td>x</td>
         <td>x</td>
      </tr>
      <tr>
         <td>12-Aug</td>
         <td>1</td>
         <td>3</td>
         <td>Intro to Git</td>
         <td>Git Labs</td>
         <td>Curran(Lead)/Cammarata</td>
         <td>12-Aug</td>
         <td>x</td>
         <td>x</td>
         <td>x</td>
      </tr>
      <tr>
         <td>13-Aug</td>
         <td>1</td>
         <td>4</td>
         <td>Agile</td>
         <td>Overview of Agile Methodology</td>
         <td>Davis/Vogel/Patton</td>
         <td>13-Aug</td>
         <td>x</td>
         <td>x</td>
         <td>x</td>
      </tr>
      <tr>
         <td>14-Aug</td>
         <td>1</td>
         <td>5</td>
         <td>Pseudocode</td>
         <td>Overview of Computer Programming, Pseudocode, Structure</td>
         <td>Davis(Lead)/Guerra/Patton</td>
         <td>14-Aug</td>
         <td>x</td>
         <td>x</td>
         <td>x</td>
      </tr>
      <tr>
         <td>17-Aug</td>
         <td>2</td>
         <td>6</td>
         <td>Pseudocode</td>
         <td>Structure, Logical Thinking, Looping, Arrays</td>
         <td>Davis(Lead)/Guerra/Patton</td>
         <td>17-Aug</td>
         <td>x</td>
         <td>x</td>
         <td>x</td>
      </tr>
      <tr>
         <td>18-Aug</td>
         <td>2</td>
         <td>7</td>
         <td>Python/Debugging</td>
         <td>Introduction / Variables / Intro to Debugging</td>
         <td>Curran(Lead)/Cammarata</td>
         <td>18-Aug</td>
         <td>x</td>
         <td>x</td>
         <td>x</td>
      </tr>
      <tr>
         <td>19-Aug</td>
         <td>2</td>
         <td>8</td>
         <td>Python</td>
         <td>Variables, Strings, input()</td>
         <td>Curran(Lead)/Cammarata</td>
         <td>19-Aug</td>
         <td>x</td>
         <td>x</td>
         <td>x</td>
      </tr>
      <tr>
         <td>20-Aug</td>
         <td>2</td>
         <td>9</td>
         <td>Python</td>
         <td>print(), Print Formatting, Operators</td>
         <td>Curran(Lead)/Cammarata</td>
         <td>20-Aug</td>
         <td>x</td>
         <td>x</td>
         <td>x</td>
      </tr>
      <tr>
         <td>21-Aug</td>
         <td>2</td>
         <td>10</td>
         <td>Python</td>
         <td>Decision Structures (If-Else), Functions</td>
         <td>Curran(Lead)/Cammarata</td>
         <td>21-Aug</td>
         <td>x</td>
         <td>x</td>
         <td>x</td>
      </tr>
      <tr>
         <td>24-Aug</td>
         <td>3</td>
         <td>11</td>
         <td>Python</td>
         <td>Repetition Structures(While and For loops)</td>
         <td>Curran(Lead)/Cammarata</td>
         <td>24-Aug</td>
         <td>x</td>
         <td>x</td>
         <td>x</td>
      </tr>
      <tr>
         <td>25-Aug</td>
         <td>3</td>
         <td>12</td>
         <td>Python</td>
         <td>File Access</td>
         <td>Curran(Lead)/Cammarata</td>
         <td>25-Aug</td>
         <td>x</td>
         <td>x</td>
         <td>x</td>
      </tr>
      <tr>
         <td>26-Aug</td>
         <td>3</td>
         <td>13</td>
         <td>Python</td>
         <td>Lists and Tuples</td>
         <td>Curran(Lead)/Cammarata</td>
         <td>26-Aug</td>
         <td>x</td>
         <td>x</td>
         <td>x</td>
      </tr>
      <tr>
         <td>27-Aug</td>
         <td>3</td>
         <td>14</td>
         <td>Python</td>
         <td>Dictionaries and Sets</td>
         <td>Curran(Lead)/Cammarata</td>
         <td>27-Aug</td>
         <td>x</td>
         <td>x</td>
         <td>x</td>
      </tr>
      <tr>
         <td>28-Aug</td>
         <td>3</td>
         <td>15</td>
         <td>Python</td>
         <td>Pickling with Dictionaries (Serialization)</td>
         <td>Curran(Lead)/Cammarata</td>
         <td>28-Aug</td>
         <td>x</td>
         <td>x</td>
         <td>x</td>
      </tr>
      <tr>
         <td>31-Aug</td>
         <td>4</td>
         <td>16</td>
         <td>Python</td>
         <td>Pickling Exercise Continued, Hangman Game Start</td>
         <td>Curran(Lead)/Cammarata</td>
         <td>31-Aug</td>
         <td>x</td>
         <td>x</td>
         <td>x</td>
      </tr>
      <tr>
         <td>1-Sep</td>
         <td>4</td>
         <td>17</td>
         <td>Python</td>
         <td>Classes and OOP</td>
         <td>Cammarata(Lead)/Curran</td>
         <td>1-Sep</td>
         <td>x</td>
         <td>x</td>
         <td>x</td>
      </tr>
      <tr>
         <td>2-Sep</td>
         <td>4</td>
         <td>18</td>
         <td>Python</td>
         <td>Classes and OOP</td>
         <td>Cammarata(Lead)/Curran</td>
         <td>2-Sep</td>
         <td>x</td>
         <td>x</td>
         <td>x</td>
      </tr>
      <tr>
         <td>3-Sep</td>
         <td>4</td>
         <td>19</td>
         <td>Python</td>
         <td>Classes and OOP</td>
         <td>Cammarata(Lead)/Curran</td>
         <td>3-Sep</td>
         <td>x</td>
         <td>x</td>
         <td>x</td>
      </tr>
      <tr>
         <td>4-Sep</td>
         <td>4</td>
         <td>20</td>
         <td>Python</td>
         <td>Classes - Inheritance, Polymorphism</td>
         <td>Cammarata(Lead)/Curran</td>
         <td>4-Sep</td>
         <td>x</td>
         <td>x</td>
         <td>x</td>
      </tr>
      <tr>
         <td>7-Sep</td>
         <td>5</td>
         <td>21</td>
         <td>Python</td>
         <td>Labor Day Holiday</td>
         <td>Cammarata(Lead)/Curran</td>
         <td>7-Sep</td>
         <td>x</td>
         <td>x</td>
         <td>x</td>
      </tr>
      <tr>
         <td>8-Sep</td>
         <td>5</td>
         <td>22</td>
         <td>Python</td>
         <td>Closures, Iterators, Generators; List Comprehension, Lambda Functions</td>
         <td>Cammarata(Lead)/Curran</td>
         <td>8-Sep</td>
         <td>x</td>
         <td>x</td>
         <td>x</td>
      </tr>
      <tr>
         <td>9-Sep</td>
         <td>5</td>
         <td>23</td>
         <td>Python</td>
         <td>Refactoring Code Using map(), zip(), enumerate(), Other Comprehensions</td>
         <td>Cammarata(Lead)/Curran</td>
         <td>9-Sep</td>
         <td>x</td>
         <td>x</td>
         <td>x</td>
      </tr>
      <tr>
         <td>10-Sep</td>
         <td>5</td>
         <td>24</td>
         <td>Python</td>
         <td>Recursion, Debugging</td>
         <td>Cammarata(Lead)/Curran</td>
         <td>11-Sep</td>
         <td>x</td>
         <td>x</td>
         <td>x</td>
      </tr>
      <tr>
         <td>11-Sep</td>
         <td>5</td>
         <td>25</td>
         <td>Python</td>
         <td>regex</td>
         <td>Cammarata(Lead)/Curran</td>
         <td>10-Sep</td>
         <td>x</td>
         <td>x</td>
         <td>x</td>
      </tr>
      <tr>
         <td>14-Sep</td>
         <td>6</td>
         <td>26</td>
         <td>Python</td>
         <td>Start Group Project</td>
         <td>Curran(Lead)/Cammarata</td>
         <td>14-Sep</td>
         <td>x</td>
         <td>x</td>
         <td>x</td>
      </tr>
      <tr>
         <td>15-Sep</td>
         <td>6</td>
         <td>27</td>
         <td>Python</td>
         <td>Group Project</td>
         <td>Curran(Lead)/Cammarata</td>
         <td>15-Sep</td>
         <td>x</td>
         <td>x</td>
         <td>x</td>
      </tr>
      <tr>
         <td>16-Sep</td>
         <td>6</td>
         <td>28</td>
         <td>Python</td>
         <td>Group Project, Multithreading, Unit Testing,  Ctypes</td>
         <td>Curran(Lead)/Cammarata</td>
         <td>16-Sep</td>
         <td>x</td>
         <td>x</td>
         <td>x</td>
      </tr>
      <tr>
         <td>17-Sep</td>
         <td>6</td>
         <td>29</td>
         <td>Python</td>
         <td>Python Basics Test</td>
         <td>Curran(Lead)/Cammarata</td>
         <td>17-Sep</td>
         <td>x</td>
         <td>x</td>
         <td>x</td>
      </tr>
      <tr>
         <td>18-Sep</td>
         <td>6</td>
         <td>30</td>
         <td>Python Data Structures &amp; Algorithms</td>
         <td>Big O Notation, Searching Algorithms</td>
         <td>Curran(Lead)/Cammarata</td>
         <td>18-Sep</td>
         <td>x</td>
         <td>x</td>
         <td>x</td>
      </tr>
      <tr>
         <td>21-Sep</td>
         <td>7</td>
         <td>31</td>
         <td>Python Data Structures &amp; Algorithms</td>
         <td>Sorting Algorithms</td>
         <td>Curran(Lead)/Cammarata</td>
         <td>21-Sep</td>
         <td>x</td>
         <td>x</td>
         <td>x</td>
      </tr>
      <tr>
         <td>22-Sep</td>
         <td>7</td>
         <td>32</td>
         <td>Python Data Structures &amp; Algorithms</td>
         <td>Linked Lists</td>
         <td>Curran(Lead)/Cammarata</td>
         <td>22-Sep</td>
         <td>x</td>
         <td>x</td>
         <td>x</td>
      </tr>
      <tr>
         <td>23-Sep</td>
         <td>7</td>
         <td>33</td>
         <td>Python Data Structures &amp; Algorithms</td>
         <td>Linked Lists, Stacks, Queues</td>
         <td>Curran(Lead)/Cammarata</td>
         <td>23-Sep</td>
         <td>x</td>
         <td>x</td>
         <td>x</td>
      </tr>
      <tr>
         <td>24-Sep</td>
         <td>7</td>
         <td>34</td>
         <td>Python Data Structures &amp; Algorithms</td>
         <td>Trees</td>
         <td>Curran(Lead)/Cammarata</td>
         <td>24-Sep</td>
         <td>x</td>
         <td>x</td>
         <td>x</td>
      </tr>
      <tr>
         <td>25-Sep</td>
         <td>7</td>
         <td>35</td>
         <td>Networking Concepts</td>
         <td>Intro to Networking/TCP/IP Model</td>
         <td>Guerra(Lead)/Curran/Cammarata</td>
         <td>25-Sep</td>
         <td>x</td>
         <td>x</td>
         <td>x</td>
      </tr>
      <tr>
         <td>28-Sep</td>
         <td>8</td>
         <td>36</td>
         <td>Networking Concepts</td>
         <td>Layer 2/NetcatWireshark intro  ( Netcat labs) (Wireshark labs)</td>
         <td>Guerra(Lead)/Curran/Cammarata</td>
         <td>28-Sep</td>
         <td>x</td>
         <td>x</td>
         <td>x</td>
      </tr>
      <tr>
         <td>29-Sep</td>
         <td>8</td>
         <td>37</td>
         <td>Networking Concepts</td>
         <td>Intro to Sockets/Layer 3/Scapy modules(Continue Wireshark)</td>
         <td>Guerra(Lead)/Curran/Cammarata</td>
         <td>29-Sep</td>
         <td>x</td>
         <td>x</td>
         <td>x</td>
      </tr>
      <tr>
         <td>30-Sep</td>
         <td>8</td>
         <td>38</td>
         <td>Networking Concepts</td>
         <td>Intro to Sockets/Layer 3/Scapy modules(Continue Wireshark) challenges</td>
         <td>Guerra(Lead)/Curran/Cammarata</td>
         <td>30-Sep</td>
         <td>x</td>
         <td>x</td>
         <td>x</td>
      </tr>
      <tr>
         <td>1-Oct</td>
         <td>8</td>
         <td>39</td>
         <td>Networking Concepts</td>
         <td>Layer 4 / TCP/UDP/ Network challenges</td>
         <td>Guerra(Lead)/Curran/Cammarata</td>
         <td>1-Oct</td>
         <td>x</td>
         <td>x</td>
         <td>x</td>
      </tr>
      <tr>
         <td>2-Oct</td>
         <td>8</td>
         <td>40</td>
         <td>Networking Socket Programming</td>
         <td>Layer 4 / TCP/UDP/ Network challenges</td>
         <td>Guerra(Lead)/Curran/Cammarata</td>
         <td>2-Oct</td>
         <td>x</td>
         <td>x</td>
         <td>x</td>
      </tr>
      <tr>
         <td>5-Oct</td>
         <td>9</td>
         <td>41</td>
         <td>Networking Socket Programming</td>
         <td>Layer 7 / Crypto/XOR/ Requests/BS4 modules</td>
         <td>Guerra(Lead)/Curran/Cammarata</td>
         <td>5-Oct</td>
         <td>x</td>
         <td>x</td>
         <td>x</td>
      </tr>
      <tr>
         <td>6-Oct</td>
         <td>9</td>
         <td>42</td>
         <td>Networking Socket Programming</td>
         <td>Layer 7 / Crypto/XOR/ Requests/BS4 modules</td>
         <td>Guerra(Lead)/Curran/Cammarata</td>
         <td>6-Oct</td>
         <td>x</td>
         <td>x</td>
         <td>x</td>
      </tr>
      <tr>
         <td>7-Oct</td>
         <td>9</td>
         <td>43</td>
         <td>Networking Socket Programming</td>
         <td>Socket Exercises</td>
         <td>Guerra(Lead)/Curran/Cammarata</td>
         <td>7-Oct</td>
         <td>x</td>
         <td>x</td>
         <td>x</td>
      </tr>
      <tr>
         <td>8-Oct</td>
         <td>9</td>
         <td>44</td>
         <td>Networking Socket Programming</td>
         <td>Socket Exercises  (Network Tool Building)</td>
         <td>Guerra(Lead)/Curran/Cammarata</td>
         <td>8-Oct</td>
         <td>x</td>
         <td>x</td>
         <td>x</td>
      </tr>
      <tr>
         <td>9-Oct</td>
         <td>9</td>
         <td>45</td>
         <td>Python/Network Final Exam</td>
         <td>Knowledge / Performance Exam</td>
         <td>Curran/Cammarata/Guerra</td>
         <td>9-Oct</td>
         <td></td>
         <td></td>
         <td></td>
      </tr>
      <tr>
         <td>12-Oct</td>
         <td>10</td>
         <td>46</td>
         <td>C Programming Basics</td>
         <td>Columbus Day Holiday</td>
         <td>Patton(Lead)/Guerra/Loomis</td>
         <td>12-Oct</td>
         <td></td>
         <td></td>
         <td></td>
      </tr>
      <tr>
         <td>13-Oct</td>
         <td>10</td>
         <td>47</td>
         <td>C Programming Basics</td>
         <td>Introduction, Variables, Arrays/Strings</td>
         <td>Patton(Lead)/Guerra/Loomis</td>
         <td>13-Oct</td>
         <td></td>
         <td></td>
         <td></td>
      </tr>
      <tr>
         <td>14-Oct</td>
         <td>10</td>
         <td>48</td>
         <td>C Programming Basics</td>
         <td>Arrays/Strings</td>
         <td>Patton(Lead)/Guerra/Loomis</td>
         <td>14-Oct</td>
         <td></td>
         <td></td>
         <td></td>
      </tr>
      <tr>
         <td>15-Oct</td>
         <td>10</td>
         <td>49</td>
         <td>C Programming Basics</td>
         <td>I/O</td>
         <td>Patton(Lead)/Guerra/Loomis</td>
         <td>15-Oct</td>
         <td></td>
         <td></td>
         <td></td>
      </tr>
      <tr>
         <td>16-Oct</td>
         <td>10</td>
         <td>50</td>
         <td>C Programming Basics</td>
         <td>I/O, Operators and Expressions</td>
         <td>Patton(Lead)/Guerra/Loomis</td>
         <td>16-Oct</td>
         <td></td>
         <td></td>
         <td></td>
      </tr>
      <tr>
         <td>19-Oct</td>
         <td>11</td>
         <td>51</td>
         <td>C Programming Basics</td>
         <td>Operators and Expressions, Bitwise Operators</td>
         <td>Patton(Lead)/Guerra/Loomis</td>
         <td>19-Oct</td>
         <td></td>
         <td></td>
         <td></td>
      </tr>
      <tr>
         <td>20-Oct</td>
         <td>11</td>
         <td>52</td>
         <td>C Programming Basics</td>
         <td>Control Flow</td>
         <td>Patton(Lead)/Guerra/Loomis</td>
         <td>20-Oct</td>
         <td></td>
         <td></td>
         <td></td>
      </tr>
      <tr>
         <td>21-Oct</td>
         <td>11</td>
         <td>53</td>
         <td>C Programming Basics</td>
         <td>Control Flow</td>
         <td>Patton(Lead)/Guerra/Loomis</td>
         <td>21-Oct</td>
         <td></td>
         <td></td>
         <td></td>
      </tr>
      <tr>
         <td>22-Oct</td>
         <td>11</td>
         <td>54</td>
         <td>C Programming Basics</td>
         <td>Functions</td>
         <td>Patton(Lead)/Guerra/Loomis</td>
         <td>22-Oct</td>
         <td></td>
         <td></td>
         <td></td>
      </tr>
      <tr>
         <td>23-Oct</td>
         <td>11</td>
         <td>55</td>
         <td>C Programming Basics</td>
         <td>Functions</td>
         <td>Patton(Lead)/Guerra/Loomis</td>
         <td>23-Oct</td>
         <td></td>
         <td></td>
         <td></td>
      </tr>
      <tr>
         <td>26-Oct</td>
         <td>12</td>
         <td>56</td>
         <td>C Programming Basics</td>
         <td>C Compiler</td>
         <td>Patton(Lead)/Guerra/Loomis</td>
         <td>26-Oct</td>
         <td></td>
         <td></td>
         <td></td>
      </tr>
      <tr>
         <td>27-Oct</td>
         <td>12</td>
         <td>57</td>
         <td>C Programming Basics</td>
         <td>Preprocessor</td>
         <td>Patton(Lead)/Guerra/Loomis</td>
         <td>27-Oct</td>
         <td></td>
         <td></td>
         <td></td>
      </tr>
      <tr>
         <td>28-Oct</td>
         <td>12</td>
         <td>58</td>
         <td>C Programming Basics</td>
         <td>Pointers and Arrays</td>
         <td>Patton(Lead)/Guerra/Loomis</td>
         <td>28-Oct</td>
         <td></td>
         <td></td>
         <td></td>
      </tr>
      <tr>
         <td>29-Oct</td>
         <td>12</td>
         <td>59</td>
         <td>C Programming Basics</td>
         <td>Pointers and Arrays</td>
         <td>Patton(Lead)/Guerra/Loomis</td>
         <td>29-Oct</td>
         <td></td>
         <td></td>
         <td></td>
      </tr>
      <tr>
         <td>30-Oct</td>
         <td>12</td>
         <td>60</td>
         <td>C Programming Basics</td>
         <td>I/O Part 2/Error Handling</td>
         <td>Patton(Lead)/Guerra/Loomis</td>
         <td>30-Oct</td>
         <td></td>
         <td></td>
         <td></td>
      </tr>
      <tr>
         <td>2-Nov</td>
         <td>13</td>
         <td>61</td>
         <td>C Programming Basics</td>
         <td>C Basics Knowledge / Performance</td>
         <td>ALL</td>
         <td></td>
         <td></td>
         <td></td>
         <td></td>
      </tr>
      <tr>
         <td>3-Nov</td>
         <td>13</td>
         <td>62</td>
         <td>C Programming</td>
         <td>Agile Review / Team Projects</td>
         <td>ALL</td>
         <td></td>
         <td></td>
         <td></td>
         <td></td>
      </tr>
      <tr>
         <td>4-Nov</td>
         <td>13</td>
         <td>63</td>
         <td>C Programming</td>
         <td>Team Projects</td>
         <td>ALL</td>
         <td></td>
         <td></td>
         <td></td>
         <td></td>
      </tr>
      <tr>
         <td>5-Nov</td>
         <td>13</td>
         <td>64</td>
         <td>C Programming</td>
         <td>Team Projects</td>
         <td>ALL</td>
         <td></td>
         <td></td>
         <td></td>
         <td></td>
      </tr>
      <tr>
         <td>6-Nov</td>
         <td>13</td>
         <td>65</td>
         <td>C Programming</td>
         <td>Team Projects</td>
         <td>ALL</td>
         <td></td>
         <td></td>
         <td></td>
         <td></td>
      </tr>
      <tr>
         <td>9-Nov</td>
         <td>14</td>
         <td>66</td>
         <td>C Programming</td>
         <td>Team Projects</td>
         <td>ALL</td>
         <td></td>
         <td></td>
         <td></td>
         <td></td>
      </tr>
      <tr>
         <td>10-Nov</td>
         <td>14</td>
         <td>67</td>
         <td>C Programming</td>
         <td>Team Projects</td>
         <td>ALL</td>
         <td></td>
         <td></td>
         <td></td>
         <td></td>
      </tr>
      <tr>
         <td>11-Nov</td>
         <td>14</td>
         <td>68</td>
         <td>C Programming</td>
         <td>Veterans Day Holiday</td>
         <td>Holiday</td>
         <td></td>
         <td></td>
         <td></td>
         <td></td>
      </tr>
      <tr>
         <td>12-Nov</td>
         <td>14</td>
         <td>69</td>
         <td>C Programming</td>
         <td>Team Projects</td>
         <td>ALL</td>
         <td></td>
         <td></td>
         <td></td>
         <td></td>
      </tr>
      <tr>
         <td>13-Nov</td>
         <td>14</td>
         <td>70</td>
         <td>C Programming</td>
         <td>Team Projects</td>
         <td>ALL</td>
         <td></td>
         <td></td>
         <td></td>
         <td></td>
      </tr>
      <tr>
         <td>16-Nov</td>
         <td>15</td>
         <td>71</td>
         <td>C Programming</td>
         <td>Team Projects</td>
         <td>ALL</td>
         <td></td>
         <td></td>
         <td></td>
         <td></td>
      </tr>
      <tr>
         <td>17-Nov</td>
         <td>15</td>
         <td>72</td>
         <td>C Programming</td>
         <td>Team Projects</td>
         <td>ALL</td>
         <td></td>
         <td></td>
         <td></td>
         <td></td>
      </tr>
      <tr>
         <td>18-Nov</td>
         <td>15</td>
         <td>73</td>
         <td>C - Data Structures &amp; Algorithms</td>
         <td>Dynamic Memory Allocation</td>
         <td>Guerra(Lead)/Patton</td>
         <td></td>
         <td>x</td>
         <td></td>
         <td></td>
      </tr>
      <tr>
         <td>19-Nov</td>
         <td>15</td>
         <td>74</td>
         <td>C - Data Structures &amp; Algorithms</td>
         <td>Linked Lists, Stack and Queues</td>
         <td>Guerra(Lead)/Patton</td>
         <td></td>
         <td>x</td>
         <td></td>
         <td></td>
      </tr>
      <tr>
         <td>20-Nov</td>
         <td>15</td>
         <td>75</td>
         <td>C - Data Structures &amp; Algorithms</td>
         <td>Big O Notation, Searching Algorithms</td>
         <td>Guerra(Lead)/Patton</td>
         <td></td>
         <td></td>
         <td></td>
         <td></td>
      </tr>
      <tr>
         <td>23-Nov</td>
         <td>16</td>
         <td>76</td>
         <td>C - Data Structures &amp; Algorithms</td>
         <td>Sorting Algorithms</td>
         <td>Guerra(Lead)/Patton</td>
         <td></td>
         <td>x</td>
         <td></td>
         <td></td>
      </tr>
      <tr>
         <td>24-Nov</td>
         <td>16</td>
         <td>77</td>
         <td>C - Data Structures &amp; Algorithms</td>
         <td>Trees, Advanced Linked Lists</td>
         <td>Guerra(Lead)/Patton</td>
         <td></td>
         <td></td>
         <td></td>
         <td></td>
      </tr>
      <tr>
         <td>25-Nov</td>
         <td>16</td>
         <td>78</td>
         <td>C Programming Review/EOC</td>
         <td>C Performance Final Knowledge/ Performance</td>
         <td>ALL</td>
         <td></td>
         <td></td>
         <td></td>
         <td></td>
      </tr>
      <tr>
         <td>26-Nov</td>
         <td>16</td>
         <td>79</td>
         <td>Assembly</td>
         <td>Thanksgiving Holiday</td>
         <td>Holiday</td>
         <td></td>
         <td></td>
         <td></td>
         <td></td>
      </tr>
      <tr>
         <td>27-Nov</td>
         <td>16</td>
         <td>80</td>
         <td>Assembly</td>
         <td>Family Day</td>
         <td>Holiday</td>
         <td></td>
         <td></td>
         <td></td>
         <td></td>
      </tr>
      <tr>
         <td>30-Nov</td>
         <td>17</td>
         <td>81</td>
         <td>Assembly</td>
         <td>Debugging, Intro to ASM</td>
         <td>Guerra(Lead)/Davis/Cammarata</td>
         <td></td>
         <td>x</td>
         <td></td>
         <td></td>
      </tr>
      <tr>
         <td>1-Dec</td>
         <td>17</td>
         <td>82</td>
         <td>Assembly</td>
         <td>ASM_Basic_Operations</td>
         <td>Guerra/Davis(Lead)/Cammarata</td>
         <td></td>
         <td>x</td>
         <td></td>
         <td></td>
      </tr>
      <tr>
         <td>2-Dec</td>
         <td>17</td>
         <td>83</td>
         <td>Assembly</td>
         <td>ASM_Control_Flow</td>
         <td>Guerra/Davis(Lead)/Cammarata</td>
         <td></td>
         <td>x</td>
         <td></td>
         <td></td>
      </tr>
      <tr>
         <td>3-Dec</td>
         <td>17</td>
         <td>84</td>
         <td>Assembly</td>
         <td>ASM Student Labs</td>
         <td>Guerra/Davis(Lead)/Cammarata</td>
         <td></td>
         <td>x</td>
         <td></td>
         <td></td>
      </tr>
      <tr>
         <td>4-Dec</td>
         <td>17</td>
         <td>85</td>
         <td>Assembly</td>
         <td>Linux Assembly Programming</td>
         <td>Guerra/Davis(Lead)/Cammarata</td>
         <td></td>
         <td>x</td>
         <td></td>
         <td></td>
      </tr>
      <tr>
         <td>7-Dec</td>
         <td>18</td>
         <td>86</td>
         <td>Assembly</td>
         <td>Windows Assembly Programming / Knowledge Exam</td>
         <td>Guerra(Lead)/Davis/Cammarata</td>
         <td></td>
         <td></td>
         <td></td>
         <td></td>
      </tr>
      <tr>
         <td>8-Dec</td>
         <td>18</td>
         <td>87</td>
         <td>Mock Evaluation</td>
         <td>REVIEW (Mock Eval Environment EXAM)</td>
         <td>All</td>
         <td></td>
         <td></td>
         <td></td>
         <td></td>
      </tr>
      <tr>
         <td>9-Dec</td>
         <td>18</td>
         <td>88</td>
         <td>Mock Evaluation</td>
         <td>Mock Eval Environment EXAM</td>
         <td>All</td>
         <td></td>
         <td></td>
         <td></td>
      </tr>
   </tbody>
</table>

